var classlab7__motordriver_1_1MotorDriver =
[
    [ "__init__", "classlab7__motordriver_1_1MotorDriver.html#a8550a4423a79ffb0aca950535f864e22", null ],
    [ "disable", "classlab7__motordriver_1_1MotorDriver.html#ad4ad26d726800b1e94746cd3facda6a4", null ],
    [ "enable", "classlab7__motordriver_1_1MotorDriver.html#a8e681b71c788a37a036a284563f130ae", null ],
    [ "set_duty", "classlab7__motordriver_1_1MotorDriver.html#a44673c4fb1964ae199238b87566a4f7b", null ],
    [ "duty", "classlab7__motordriver_1_1MotorDriver.html#ae6aeb370c012059a99fe4c9a965708a3", null ],
    [ "IN1_pin", "classlab7__motordriver_1_1MotorDriver.html#a75029dbccd5e0f7d4cba0d468832b9f3", null ],
    [ "IN2_pin", "classlab7__motordriver_1_1MotorDriver.html#a679630efdffb8e134aa4658c2948c2fb", null ],
    [ "nSleep_pin", "classlab7__motordriver_1_1MotorDriver.html#a838ec2f7a4f05a59a4a578539e9fa00a", null ],
    [ "t3ch1", "classlab7__motordriver_1_1MotorDriver.html#a2ce40a18b11bada6a3b87f4173a96c8d", null ],
    [ "t3ch2", "classlab7__motordriver_1_1MotorDriver.html#a8f71179ef249d2a9d15d4864fc7a9612", null ],
    [ "timer", "classlab7__motordriver_1_1MotorDriver.html#ab09792f44401a0bed1a962e56c6b9016", null ]
];