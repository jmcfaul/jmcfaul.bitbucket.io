/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Jackson McFaul's Portfolio", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "Lab01", "index.html#sec_lab01", null ],
    [ "Lecture HW1", "index.html#sec_Lecture", null ],
    [ "Lab02", "index.html#sec_lab02", null ],
    [ "Lab03", "index.html#sec_lab03", null ],
    [ "Lab04", "index.html#sec_lab04", null ],
    [ "Lab05", "index.html#sec_lab05", null ],
    [ "Lab06 Part 1", "index.html#sec_lab06-1", null ],
    [ "Lab06 Part 2", "index.html#sec_lab06-2", null ],
    [ "Lab07", "index.html#sec_lab07", null ],
    [ "Lab 06 Kp Tuning Plots", "page_Lab06-Part2.html", [
      [ "Methodology", "page_Lab06-Part2.html#sec_method", null ],
      [ "Motor Speed vs Time plots", "page_Lab06-Part2.html#sec_plots", null ],
      [ "Kp = .25", "page_Lab06-Part2.html#sec_plot1", null ],
      [ "Kp = .50", "page_Lab06-Part2.html#sec_plot2", null ],
      [ "Kp = .75", "page_Lab06-Part2.html#sec_plot3", null ],
      [ "Kp = 1.00", "page_Lab06-Part2.html#sec_plot4", null ],
      [ "Kp = 3.00", "page_Lab06-Part2.html#sec_plot5", null ],
      [ "Kp = 5.00", "page_Lab06-Part2.html#sec_plot6", null ],
      [ "Kp = 10.00", "page_Lab06-Part2.html#sec_plot7", null ]
    ] ],
    [ "Lab 07 Response Curves", "page_Lab07plots.html", [
      [ "Introduction", "page_Lab07plots.html#sec_intro2", null ],
      [ "Methodology", "page_Lab07plots.html#sec_method2", null ],
      [ "Reference and Response Plots", "page_Lab07plots.html#sec_plots2", null ],
      [ "[Kp = 0.1] [J = 1.19E9]", "page_Lab07plots.html#sec_resp1", null ],
      [ "[Kp = 0.20] [J = 1.01E9]", "page_Lab07plots.html#sec_resp2", null ],
      [ "[Kp = 0.30] [J = 1.12E9]", "page_Lab07plots.html#sec_resp3", null ],
      [ "[Kp = 0.25] [J = 1.08E9]", "page_Lab07plots.html#sec_resp4", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"",
"lab4__main_8py.html#a8b41273d499e42502f020e7e5b6ce198"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';