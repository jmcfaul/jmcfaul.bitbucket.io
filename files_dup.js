var files_dup =
[
    [ "closedloop.py", "closedloop_8py.html", [
      [ "ClosedLoop", "classclosedloop_1_1ClosedLoop.html", "classclosedloop_1_1ClosedLoop" ]
    ] ],
    [ "controllertask.py", "controllertask_8py.html", [
      [ "controller", "classcontrollertask_1_1controller.html", "classcontrollertask_1_1controller" ]
    ] ],
    [ "controllertask_lab7.py", "controllertask__lab7_8py.html", [
      [ "controller", "classcontrollertask__lab7_1_1controller.html", "classcontrollertask__lab7_1_1controller" ]
    ] ],
    [ "elevatormain.py", "elevatormain_8py.html", "elevatormain_8py" ],
    [ "Encoder.py", "Encoder_8py.html", [
      [ "ENCODER", "classEncoder_1_1ENCODER.html", "classEncoder_1_1ENCODER" ]
    ] ],
    [ "FSM_elevator.py", "FSM__elevator_8py.html", [
      [ "TaskElevator", "classFSM__elevator_1_1TaskElevator.html", "classFSM__elevator_1_1TaskElevator" ],
      [ "Button", "classFSM__elevator_1_1Button.html", "classFSM__elevator_1_1Button" ],
      [ "MotorDriver", "classFSM__elevator_1_1MotorDriver.html", "classFSM__elevator_1_1MotorDriver" ]
    ] ],
    [ "lab01.py", "lab01_8py.html", "lab01_8py" ],
    [ "lab2FSM.py", "lab2FSM_8py.html", [
      [ "TaskLEDcontrol", "classlab2FSM_1_1TaskLEDcontrol.html", "classlab2FSM_1_1TaskLEDcontrol" ]
    ] ],
    [ "lab2main.py", "lab2main_8py.html", "lab2main_8py" ],
    [ "lab3main.py", "lab3main_8py.html", "lab3main_8py" ],
    [ "lab4_Encoder.py", "lab4__Encoder_8py.html", [
      [ "ENCODER", "classlab4__Encoder_1_1ENCODER.html", "classlab4__Encoder_1_1ENCODER" ]
    ] ],
    [ "lab4_encoderdriver.py", "lab4__encoderdriver_8py.html", [
      [ "runencoder", "classlab4__encoderdriver_1_1runencoder.html", "classlab4__encoderdriver_1_1runencoder" ]
    ] ],
    [ "lab4_main.py", "lab4__main_8py.html", "lab4__main_8py" ],
    [ "lab5_BLEFSM.py", "lab5__BLEFSM_8py.html", [
      [ "BLEFSM", "classlab5__BLEFSM_1_1BLEFSM.html", "classlab5__BLEFSM_1_1BLEFSM" ]
    ] ],
    [ "lab5main.py", "lab5main_8py.html", "lab5main_8py" ],
    [ "lab6_encoder.py", "lab6__encoder_8py.html", [
      [ "encoder", "classlab6__encoder_1_1encoder.html", "classlab6__encoder_1_1encoder" ]
    ] ],
    [ "lab6_motordriver.py", "lab6__motordriver_8py.html", [
      [ "MotorDriver", "classlab6__motordriver_1_1MotorDriver.html", "classlab6__motordriver_1_1MotorDriver" ]
    ] ],
    [ "lab6_UI_frontend.py", "lab6__UI__frontend_8py.html", "lab6__UI__frontend_8py" ],
    [ "lab7_motordriver.py", "lab7__motordriver_8py.html", [
      [ "MotorDriver", "classlab7__motordriver_1_1MotorDriver.html", "classlab7__motordriver_1_1MotorDriver" ]
    ] ],
    [ "shared_variables.py", "shared__variables_8py.html", "shared__variables_8py" ],
    [ "userinterface.py", "userinterface_8py.html", [
      [ "TaskUser", "classuserinterface_1_1TaskUser.html", "classuserinterface_1_1TaskUser" ]
    ] ]
];