var searchData=
[
  ['set_5fduty_116',['set_duty',['../classlab6__motordriver_1_1MotorDriver.html#a8e64b6300cfe80a8e909769fd4614e89',1,'lab6_motordriver.MotorDriver.set_duty()'],['../classlab7__motordriver_1_1MotorDriver.html#a44673c4fb1964ae199238b87566a4f7b',1,'lab7_motordriver.MotorDriver.set_duty()'],['../classlab7__motordriver__test_1_1MotorDriver.html#af7e09cd3e201ed9167bc04f7acb3d30c',1,'lab7_motordriver_test.MotorDriver.set_duty()']]],
  ['set_5fkp_117',['set_Kp',['../classclosedloop_1_1ClosedLoop.html#a617a88880b37c7434947936e1d3a37ce',1,'closedloop::ClosedLoop']]],
  ['set_5fposition_118',['set_position',['../classEncoder_1_1ENCODER.html#a7761d771fe57a9b7f5207eb18ccc53f1',1,'Encoder.ENCODER.set_position()'],['../classlab4__Encoder_1_1ENCODER.html#accdb923ea21c214032df191615703d54',1,'lab4_Encoder.ENCODER.set_position()'],['../classlab6__encoder_1_1encoder.html#ab485e0645c6896d2c993352fff2539ab',1,'lab6_encoder.encoder.set_position()']]],
  ['setbuttonstate_119',['setButtonState',['../classFSM__elevator_1_1Button.html#abe169e5d5a865a95d07ab5d4e3d36357',1,'FSM_elevator::Button']]],
  ['stop_120',['STOP',['../classFSM__elevator_1_1MotorDriver.html#aa53fb3d2ab96b35f1683d87df223bcfd',1,'FSM_elevator::MotorDriver']]]
];
